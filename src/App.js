import React, { Component } from "react";

import UserInfo from "./UserInfo";

class App extends Component {
    state = { users: [], isLoading: false };

    render() {
        return (
            <div>
                <h1>Lista utenti</h1>
                <button onClick={this.loadUsers.bind(this)}>
                    Carica utenti
                </button>
                {this.loadingSpinner()}
                <ul>
                    {this.state.users.map(u => (
                        <UserInfo key={u.id} user={u} />
                    ))}
                </ul>
            </div>
        );
    }

    loadingSpinner() {
        if (this.state.isLoading) {
            return <b>Sto caricando gli utenti...</b>;
        }
    }

    loadUsers() {
        this.setState({ isLoading: true, users: [] });
        fetch("https://reqres.in/api/users")
            .then(response => response.json())
            .then(users =>
                this.setState({ users: users.data, isLoading: false })
            );
    }
}

export default App;
