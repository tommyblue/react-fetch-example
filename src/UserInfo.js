import React from "react";

import styles from "./UserInfo.module.scss";

const UserInfo = ({ user }) => (
    <li className={`${styles.pippo} ${styles.pluto}`}>
        <div className={styles.placeholder}>
            <img src={user.avatar} alt="img" />
        </div>
        {user.first_name} {user.last_name}
    </li>
);

export default UserInfo;
